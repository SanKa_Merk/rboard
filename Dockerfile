FROM python:3
ENV PYTHONUNBUFFERED=1
RUN mkdir /rboard
WORKDIR /rboard
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 8000
CMD ["sh", "start.sh"]
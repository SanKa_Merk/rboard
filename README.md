# Распознавания текста с изображения.

## Links 
| Назначение | URL |
| ------ | ------ |
| Test |  |
| Prod |  |


## Requests
| Тип запроса | URL | Описание |
| ------ | ------ | ------ |
| POST | accounts/profile/add1 | Загрузка пользователем документа с текстом и его последующая обработка |
| POST | accounts/profile/change/<int:pk> | Загрузка пользователем исправленного документа |
| POST | accounts/profile/delete/<int:pk> | Удаление пользователем документа |
| POST | accounts/login | Авторизация пользователя |
| POST | accounts/logout | Выход пользователя из учетной записи |
| GET | accounts/profile/<int:pk> | Получение информации о документе |
| GET | accounts/profile | Документы пользователя |
| POST | accounts/password/change | Смена пароля пользователем |
| POST | accounts/register | Регистрация пользователя |

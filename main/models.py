from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from .utilities import get_timestamp_path


class AdvUser(AbstractUser):
    send_messages = models.BooleanField(default=True, verbose_name='Слать распознанный текст на почту')

    class Meta(AbstractUser.Meta):
        pass

    def delete(self, *args, **kwargs):
        for doc in self.doc_set_all():
            doc.delete()
        super().delete(*args, **kwargs)

class Priority(models.Model):
    name = models.CharField(max_length=20, db_index=True, verbose_name='Название')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Приоритеты'
        verbose_name = 'Приоритет'
        ordering = ['name']

class Document(models.Model):
    image = models.ImageField(blank=True, upload_to=get_timestamp_path, verbose_name='Изображение')
    content = models.TextField(verbose_name='Содержание', blank=True)
    title = models.CharField(max_length=20, verbose_name='Название', blank=True)
    published = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Опубликовано')
    priority = models.ForeignKey(Priority, null=True, on_delete=models.PROTECT, verbose_name='Приоритет')
    author = models.ForeignKey(AdvUser, on_delete=models.CASCADE, verbose_name='Автор')

    class Meta:
        verbose_name_plural='Документы'
        verbose_name='Документ'
        ordering=['-published']





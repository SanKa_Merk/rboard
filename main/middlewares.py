from .models import Priority

def priority_context_processor(request):
    context = {}
    context['priorities'] = Priority.objects.all()
    context['all'] = ''
    if 'page' in request.GET:
        page = request.GET['page']
        if page != '1':
            if context['all']:
                context['all'] += '&page=' + page
            else:
                context['all'] = '?page=' + page
    return context

from django.urls import path

from .views import index, DocLoginView, profile, DocLogoutView, ChangeUserInfoView, DocPasswordChangeView
from .views import RegisterDoneView, RegisterUserView, detail, by_priority, profile_doc_detail, profile_doc_add, \
    profile_doc_add1, profile_doc_change, profile_doc_delete

app_name = 'main'
urlpatterns = [
    path('', index, name='index'),
    path('accounts/login/', DocLoginView.as_view(), name='login'),
    path('account/profile/change/<int:pk>', profile_doc_change, name='profile_doc_change'),
    path('account/profile/delete/<int:pk>', profile_doc_delete, name='profile_doc_delete'),
    path('accounts/profile/add', profile_doc_add, name='profile_doc_add'),
    path('accounts/profile/add1', profile_doc_add1, name='profile_doc_add1'),
    path('accounts/profile/<int:pk>/', profile_doc_detail, name='profile_doc_detail'),
    path('accounts/profile/', profile, name='profile'),
    path('account/logout',DocLogoutView.as_view(), name='logout'),
    path('accounts/profile/change/', ChangeUserInfoView.as_view(), name='profile_change'),
    path('accounts/password/change/', DocPasswordChangeView.as_view(), name='password_change'),
    path('accounts/register/done/', RegisterDoneView.as_view(), name='register_done'),
    path('accounts/register/', RegisterUserView.as_view(), name='register'),
    path('<int:status_pk>/<int:pk>/', detail, name='detail'),
    path('<int:pk>/', by_priority, name='by_priority'),
    #path('<str:page>/', other_page, name='other'),
]
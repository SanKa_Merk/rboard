from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import UpdateView, CreateView
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from django.core.paginator import Paginator
from django.shortcuts import redirect

from .models import AdvUser, Priority, Document
from .forms import ChangeUserInfoForm,RegisterUserForm, DocumentForm, DocumentImageForm

from PIL import Image
import pytesseract
import cv2

def index(request):
    return render(request, 'main/index.html')

class DocLoginView(LoginView):
    template_name = 'main/login.html'

@login_required
def profile(request):
    docs = Document.objects.filter(author=request.user.pk)
    context = {'docs': docs}
    return render(request, 'main/profile.html', context)

@login_required
def profile_doc_detail(request, pk):
    doc = get_object_or_404(Document, pk=pk)
    context = {'doc': doc}
    return render(request, 'main/profile_doc_detail.html', context)

@login_required
def profile_doc_add(request, image, text):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            doc = form.save()
            return redirect('main:profile')
    else:
        image = image
        text = text
        priority = get_object_or_404(Priority, id=2)
        form = DocumentForm(initial={'author': request.user.pk, 'priority' : priority, 'image': image, 'content': content })
    context = {'form': form}
    return render(request, 'main/profile_doc_add.html', context)


@login_required
def profile_doc_add1(request):
    if request.method == 'POST':
        form = DocumentImageForm(request.POST, request.FILES)
        if form.is_valid():
            doc = form.save(commit=False)
            pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
            doc.content = pytesseract.image_to_string(Image.open(doc.image), lang='rus')
            doc = form.save()
            doc.save()
            return redirect('main:profile_doc_change', pk=doc.pk)
        else:
            form = DocumentImageForm
    else:
        priority = get_object_or_404(Priority, name="Важное")
        form = DocumentImageForm(initial={'author': request.user.pk, 'priority': priority})
    context = {'form': form}
    return render(request, 'main/profile_doc_add1.html', context)

@login_required
def profile_doc_change(request, pk):
    doc = get_object_or_404(Document, pk=pk)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES, instance=doc)
        if form.is_valid():
            doc = form.save()
            return redirect('main:profile')
    else:
        form = DocumentForm(instance=doc)
    context = {'form': form}
    return render(request, 'main/profile_doc_change.html', context)

@login_required
def profile_doc_delete(request, pk):
    doc = get_object_or_404(Document, pk=pk)
    if request.method == 'POST':
        doc.delete()
        return redirect('main:profile')
    else:
        context = {'doc': doc}
        return render(request, 'main/profile_doc_delete.html', context)


class DocLogoutView(LoginRequiredMixin, LogoutView):
    template_name = 'main/logout.html'


class ChangeUserInfoView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = AdvUser
    template_name = 'main/change_user_info.html'
    form_class = ChangeUserInfoForm
    success_url = reverse_lazy('main:profile')
    success_message = 'Личные данные пользователя изменены'

    def dispatch(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)

class DocPasswordChangeView(SuccessMessageMixin, LoginRequiredMixin, PasswordChangeView):
    template_name = 'main/password_change.html'
    success_url = reverse_lazy('main:profile')
    success_message = 'Пароль пользователя изменен'

class RegisterUserView(CreateView):
    model = AdvUser
    template_name = 'main/register_user.html'
    form_class = RegisterUserForm
    success_url = reverse_lazy('main:register_done')

class RegisterDoneView(TemplateView):
    template_name = 'main/register_done.html'

def by_priority(request, pk):
    priority = get_object_or_404(Priority, pk=pk)
    docs = Document.objects.filter(priority=pk, author=request.user.pk)
    paginator = Paginator(docs, 2)
    if 'page' in request.GET:
        page_num = request.GET['page']
    else:
        page_num = 1
    page = paginator.get_page(page_num)
    context = {'priority': priority, 'page': page, 'docs': page.object_list}
    return render(request, 'main/by_priority.html', context)

def detail(request, priority_pk, pk):
    doc = get_object_or_404(Document, pk=pk)
    context = {'doc': doc}
    return render(request, 'main/detail.html', context)

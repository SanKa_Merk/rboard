from django.contrib import admin

from .models import AdvUser, Priority, Document

admin.site.register(AdvUser)
admin.site.register(Priority)
admin.site.register(Document)

# Register your models here.
